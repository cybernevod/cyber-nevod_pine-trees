export function animate({ timing, draw, duration, onStart = () => {}, onEnd = () => {} }) {
  onStart();
  
  const start = performance.now();
  
  requestAnimationFrame(function animate(time) {
    let timeFraction = (time - start) / duration;
    if (timeFraction > 1) timeFraction = 1;
    
    // вычисление текущего состояния анимации
    const progress = timing(timeFraction);
    
    draw(progress); // отрисовать её
    
    if (timeFraction < 1) {
      requestAnimationFrame(animate);
    } else {
      onEnd();
    }
  });
}

function makeEaseInOut(timing) {
  return function (timeFraction) {
    let process = 0;
    
    if (timeFraction < 0.5) {
      process = timing(2 * timeFraction) / 2;
    } else {
      process = (2 - timing(2 * (1 - timeFraction))) / 2;
    }
    
    return process;
  };
}

function bounce(timeFraction) {
  for (let a = 0, b = 1; 1; a += b, b /= 2) {
    if (timeFraction >= (7 - 4 * a) / 11) {
      return -(((11 - 6 * a - 11 * timeFraction) / 4) ** 2) + (b ** 2);
    }
  }
}

export const easeInOut = makeEaseInOut(bounce);
