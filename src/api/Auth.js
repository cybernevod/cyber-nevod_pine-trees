import http from '../http';

class Auth {
  static async auth(credential) {
    return await http.get('/auth/', { params: credential });
  }
}

export default Auth;
