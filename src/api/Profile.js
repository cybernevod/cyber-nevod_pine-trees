import http from '../http';

class Profile {
  static async fetch(id) {
    return await http.get(`/user/${id}/`);
  }
}

export default Profile;
