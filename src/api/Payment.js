import http from '../http';

class Payment {
  static async getLink() {
    return await http.get('/payment/');
  }
}

export default Payment;
