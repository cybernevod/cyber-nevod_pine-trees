import http from '../http';

class Contacts {
  static async get() {
    return await http.get('/contacts/');
  }
}

export default Contacts;
