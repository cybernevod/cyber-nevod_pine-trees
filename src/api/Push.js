import http from '../http';

class Push {
  static async setToken(userId, token) {
    await http.post(`/firebase/${userId}/`, {}, { params: { token } });
  }
}

export default Push;
