export default ({ units, groups, xaxisCategories }) => ({
  chart: {
    id: 'meters-data-chart',
    fontFamily: 'Play-Regular, sans-serif',
    dropShadow: {
      enabled: true,
      top: 0,
      left: 1,
      blur: 0,
      opacity: 1,
      color: '#65941F',
    },
    toolbar: {
      show: false,
    },
  },
  tooltip: {
    enabled: true,
    style: {
      fontSize: '10px',
    },
    x: {
      show: true,
      formatter: (val) => ({
        '01': 'Январь', '02': 'Февраль', '03': 'Март',
        '04': 'Апрель', '05': 'Май', '06': 'Июнь',
        '07': 'Июль', '08': 'Август', '09': 'Сентябрь',
        10: 'Октябрь', 11: 'Ноябрь', 12: 'Декабрь',
      }[val]),
    },
    y: {
      formatter: (val) => (`${Math.round(val * 10) / 10} ${units}`),
      title: {
        formatter: () => 'Расход: ',
      },
    },
    fixed: {
      enabled: true,
      position: 'top',
      offsetX: 0,
      offsetY: 0,
    },
  },
  grid: {
    enabled: true,
    borderColor: '#F4F5FB',
    xaxis: {
      lines: {
        show: true,
      },
    },
    yaxis: {
      lines: {
        show: true,
      },
    },
  },
  xaxis: {
    categories: xaxisCategories,
    labels: {
      show: true,
      style: {
        colors: ['#52525F'],
        fontSize: '8px',
        fontWeight: 400,
      },
    },
    group: {
      style: {
        fontSize: '10px',
        fontWeight: 600,
        colors: ['#52525F'],
      },
      groups,
    },
  },
  yaxis: {
    opposite: true,
    labels: {
      style: {
        colors: ['#155776'],
        fontSize: '10px',
        fontWeight: 400,
      },
      formatter: (val) => (`${Math.round(val * 10) / 10} ${units}`),
    },
  },
  plotOptions: {
    bar: {
      borderRadius: 2,
    },
  },
  dataLabels: {
    enabled: false,
  },
  fill: {
    colors: ['#A0DD46'],
    type: 'gradient',
    gradient: {
      type: 'vertical',
      gradientToColors: ['#649818'],
      inverseColors: false,
      stops: [0, 100],
    },
  },
});
