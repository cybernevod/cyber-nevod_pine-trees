export default function guest({ next, store }) {
  if (!store.getters.isAuth) {
    next();
  } else {
    next('/');
  }
}
