import screens from '@/router/types';

export default function auth({ next, store }) {
  if (store.getters.isAuth) {
    next();
  } else {
    next({ name: screens.AUTH_SCREEN });
  }
}
