module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/vue3-essential',
    '@vue/airbnb',
  ],
  parserOptions: {
    parser: '@babel/eslint-parser',
  },
  rules: {
    'no-tabs': 'off',
    'no-trailing-spaces': 'off',
    'max-len': 'off',
    'object-curly-newline': 'off',
    'no-shadow': 'off',
    'no-return-await': 'off',
    'no-unused-expressions': 'off',
    'no-param-reassign': 'off',
    'vuejs-accessibility/click-events-have-key-events': 'off',
    'vuejs-accessibility/form-control-has-label': 'off',
    'vuejs-accessibility/anchor-has-content': 'off',
    'consistent-return': 'off',
    'object-property-newline': 'off',
    'no-underscore-dangle': 'off',
    'no-plusplus': 'off',
    'import/prefer-default-export': 'off',
    'import/extensions': [
      'error',
      'ignorePackages', { js: 'never', vue: 'never' },
    ],
  },
};
